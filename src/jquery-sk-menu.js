
import { SkMenuImpl } from "../../sk-menu/src/impl/sk-menu-impl.js";

export class JquerySkMenu extends SkMenuImpl {

    get prefix() {
        return 'jquery';
    }

}
