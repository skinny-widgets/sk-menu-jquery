# Skinny Widgets Menu for Jquery Theme


menu element

```
npm i sk-menu sk-menu-jquery --save
```
```html
<sk-menu id="skMenu">
    <sk-menu-item>foo</sk-menu-item>
    <sk-menu-item>bar</sk-menu-item>
</sk-menu>
<script type="module">
    import { SkConfig } from '/node_modules/sk-config/src/sk-config.js';
    import { SkMenu } from '/node_modules/sk-menu/src/sk-menu.js';

    customElements.define('sk-config', SkConfig);
    customElements.define('sk-menu', SkMenu);
</script>
```


id: SkMenuTpl